# Chinese Sentiment Prediction

中文情緒分析 

* 使用jieba斷詞
* 使用Naive Bayes分類器(目前)
* 支援正負情緒分類
* 支援使用者自訂字典


# 安裝說明

使用 Python3 執行
```
git clone https://gitlab.com/LinZap/Sentiment_Prediction.git
pip install jieba
```

# 使用說明

### Training

將正/負訓練資料放置`data`資料夾下

* `negative.txt`: 負面情緒 dataset
* `positive.txt`: 正面情緒 dataset

執行訓練
```
cd examples
python training_example.py
```


### Testing

可以修改 `testing_example.py` 中的句子來測試資料

```
python testing_example.py
```

輸出：

```
沒做功課就來討罵[negative]
這個消息我聽說了!!很正確!![positive]
```

# CLR 

可以使用指令呼叫

```
py path/to/project/bin/prediction.py "沒做功課就來討罵"
```

輸出

```
negative
```


