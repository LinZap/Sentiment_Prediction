#coding=utf-8

import sys
import os 

currnet_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(currnet_path + '/../src')

import testing

model_path = currnet_path + '/../model/'
dic_path = currnet_path + '/../dict/user_dic.dic'

testing.load_training_data(model_path,dic_path)

def prediction(text):
	result = testing.test_sentance(text)
	if result['pos'] > result['neg']:
		print('positive')
	elif result['neg'] > result['pos']:
		print('negative')
	else:
		print('neutral')

prediction(sys.argv[1])